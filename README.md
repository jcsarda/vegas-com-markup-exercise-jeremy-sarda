# Vegas.com Markup Exercise

## Features

* [x] Built from Create React App. Uses ES6 & Webpack with Babel Support
* [x] Tabbed Content using react-tabs package.
* [x] Expand & Collapse manually in Description & Details tab panels.
* [x] Ability to use HOC at the content level to control Tabs component state at the content level. Allowing strip link to change currently selected tab.
* [x] Local API Consumption with non-blocking promises functionality via Fetch API.
* [x] Semantic & Accessible HTML Code.
* [x] Service Worker Client Caching

## Getting Started

### 1) Install Dependencies

`npm install`

### 2) Run Development w/ Live Reload

`npm start`

### 3) Build Static For Production

`npm build`
