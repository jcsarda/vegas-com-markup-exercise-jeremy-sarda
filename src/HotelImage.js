import React, {Component} from 'react';
import _ from 'lodash'

class HotelImage extends Component {

  constructor(props) {
    super(props)
    this.state = {
      requestFailed: false,
      hotelData: this.props.hotelData
    }
  }

  render() {
    return (<img src={this.getHotelImageUrl()} className="App-hotel-image" alt="logo"/>);
  }

  getHotelImageUrl() {
    if (this.state.hotelData) {
      let productMapMedia = _.find(this.state.hotelData.media, ['type', 'productImage'])
      return productMapMedia.href
    }
  }
}

export default HotelImage;
