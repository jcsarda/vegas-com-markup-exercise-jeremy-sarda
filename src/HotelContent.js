import React, {Component} from 'react';
import {Grid, Row, Col} from 'react-flexbox-grid'
import HotelTabs from "./HotelTabs";

class HotelContent extends Component {

  constructor(props) {
    super(props)
    this.state = {
      hotelData: this.props.hotelData,
      hotelTabs: <div>Loading...</div>
    }

  }

  handleLocationClick = (e) => {
    this.setState({tabIndex: 2})
  }

  /**
   *
   * Render the component.
   *
   * 1) Check for the current state's requestFailed property. Show a different
   *    element
   *
   * @returns
   * @memberof HotelList
   */
  render() {

    return (
      <Grid fluid className="App-hotel-content">
        <Row className="App-hotel-header">
          <Col xs={9} sm={9} md={9} lg={9}>
            <h1>{this.state.hotelData.name}</h1>
            <p className="App-hotel-rating">{this.starRating()}</p>
            <p className="App-hotel-info">
              <button className="App-location-link" onClick={this.handleLocationClick}>
                <i className="vegas-com-icon sym-mark"></i>{this.state.hotelData.location.areaName}
              </button>
              <button>
                <i className="vegas-com-icon sym-phone"></i>{this.state.hotelData.phoneNumber}
              </button>
              <button>
                <i className="vegas-com-icon sym-like"></i>Best Price Guarantee
              </button>
            </p>

          </Col>
          <Col xs={3} sm={3} md={3} lg={3}>
            <p className="App-hotel-price">
              ${this
                .state
                .hotelData
                .price
                .toFixed(0)}
              <span>Hotel Rooms From</span>
            </p>
          </Col>
        </Row>
        <Row>
          <Col xs={12} sm={12} md={12} lg={12}>
            <HotelTabs tabIndex={this.state.tabIndex} hotelData={this.props.hotelData}/>
          </Col>
        </Row>
      </Grid>
    );
  }

  starRating() {
    return (
      <span>
        <i className="vegas-com-icon sym-star"></i>
        <i className="vegas-com-icon sym-star"></i>
        <i className="vegas-com-icon sym-star"></i>
        <i className="vegas-com-icon sym-star"></i>
        <i className="vegas-com-icon sym-star"></i>
      </span>
    )
  }
}

export default HotelContent;
