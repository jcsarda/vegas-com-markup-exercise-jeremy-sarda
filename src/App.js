import React, {Component} from 'react';
import {Grid, Row, Col} from 'react-flexbox-grid'
import HotelImage from './HotelImage'
import HotelList from './HotelList'
import HotelContent from './HotelContent'
import './App.css';

class App extends Component {

  defaultHotelUrl = () => `/api/hotels/venetian.json`

  constructor(props) {
    super(props)
    this.state = {
      requestFailed: false,
      hotelData: null
    }
  }

  render() {
    if (this.state.requestFailed)
      return <p>Failed!</p>
    if (!this.state.hotelData)
      return <p>Loading...</p>

    console.log('HOTEL DATA FIRST LOAD', this.state.hotelData);

    return (
      <Grid fluid className="App">
        <Row className="App-top-bar">
          <Col>
            <a href="">
              <i className="vegas-com-icon circled sym-left"></i>See All Las Vegas Hotels
            </a>
          </Col>
        </Row>
        <Row>
          <Col className="App-left-col">
            <HotelImage hotelData={this.state.hotelData}/>
            <HotelList hotelData={this.state.hotelData}/>
          </Col>
          <Col className="App-right-col" xs={8}>
            <HotelContent hotelData={this.state.hotelData}/>
          </Col>
        </Row>
      </Grid>
    );
  }

  /**
   * Fetch Hotel data and add it to the current HotelContent state
   *
   * @memberof HotelContent
   */
  componentDidMount() {
    fetch(this.defaultHotelUrl()).then(response => {
      if (!response.ok) {
        throw Error("Network request failed")
      }

      return response
    })
      .then(d => d.json())
      .then(d => {
        this.setState({hotelData: d})
      }, () => {
        this.setState({requestFailed: true})
      })
  }

}

export default App;
