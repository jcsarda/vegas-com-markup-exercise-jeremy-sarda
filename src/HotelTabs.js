import React, {Component, Fragment} from 'react';
import {Tab, Tabs, TabList, TabPanel} from 'react-tabs';
import _ from 'lodash'

// const logo = require('./images/PIVX_Planet_1a_239x83.png');

class HotelTabs extends Component {

  constructor(props) {
    super(props)
    this.state = {
      hotelData: this.props.hotelData,
      descriptionCollapsed: true,
      detailsCollapsed: true,
      tabIndex: this.props.tabIndex || 0
    }
  }

  toggleDescriptionCollapsed = () => {
    this.setState({
      descriptionCollapsed: !this.state.descriptionCollapsed
    });
  }

  toggleDetailsCollapsed = () => {
    this.setState({
      detailsCollapsed: !this.state.detailsCollapsed
    });
  }

  componentWillReceiveProps(nextProps) {
    if (this.state.tabIndex !== nextProps.tabIndex) {
      this.setState({tabIndex: nextProps.tabIndex});
    }
  }

  render() {

    if (!this.state.hotelData) 
      return <p>Loading...</p>

    return (
      <Tabs
        className="App-hotel-content-tabs"
        selectedIndex={this.state.tabIndex}
        onSelect={tabIndex => this.setState({tabIndex: tabIndex})}>
        <TabList>
          <Tab>
            <span>Description</span>
          </Tab>
          <Tab>
            <span>Details</span>
          </Tab>
          <Tab>
            <span>Location</span>
          </Tab>
        </TabList>

        <TabPanel>
          <div
            className={this.state.descriptionCollapsed
            ? "collapsed"
            : "expanded"}>
            <p className="pre-line">{this.state.hotelData.description}</p>
          </div>
          <button className="App-location-link" onClick={this.toggleDescriptionCollapsed}>
            {this.state.descriptionCollapsed
              ? "Show Full Description"
              : "Hide Full Description"}
            <i
              className={this.state.descriptionCollapsed
              ? "vegas-com-icon circled sym-down"
              : "vegas-com-icon circled sym-up"}></i>
          </button>
        </TabPanel>

        <TabPanel>
          <div
            className={this.state.detailsCollapsed
            ? "collapsed"
            : "expanded"}>
            <dl className="pre-line">
              {this
                .state
                .hotelData
                .details
                .map(detail => (
                  <Fragment key={detail.label}>
                    <dt>
                      <strong>{detail.label}:</strong>
                    </dt>
                    <dd>
                      <p>{detail.value}</p>
                    </dd>
                  </Fragment>
                ))}</dl>
          </div>

          <button className="App-location-link" onClick={this.toggleDetailsCollapsed}>
            {this.state.detailsCollapsed
              ? "View More Details"
              : "View Fewer Details"}
            <i
              className={this.state.detailsCollapsed
              ? "vegas-com-icon circled sym-down"
              : "vegas-com-icon circled sym-up"}></i>
          </button>
        </TabPanel>

        <TabPanel className="App-hotel-location">
          <p>
            <i className="vegas-com-icon sym-mark"></i>{this.state.hotelData.location.address}
          </p>
          <img alt={this.state.hotelData.name} src={(this.getMapUrl())}/>
        </TabPanel>

      </Tabs>
    );
  }

  getMapUrl() {
    let productMapMedia = _.find(this.state.hotelData.media, ['type', 'productMap'])
    return productMapMedia.href
  }
}

export default HotelTabs;
