import React, {Component, Fragment} from 'react';
import _ from 'lodash'
/**
 *
 *
 * @param {*} username
 * @returns
 */

class HotelList extends Component {

  hotelListAPIUrl = () => `/api/hotels/index.json`

  constructor(props) {
    super(props)
    this.state = {
      requestFailed: false
    }
  }

  /**
   * Fetch the hotel list from the local API after the component mounts.
   * Convert the JSON response to a javascript object.
   * Update the "hotelPrices" property in the current state with the value
   * If the fetch request fails, update the "requestFailed" property in the current state
   */
  componentDidMount() {
    fetch(this.hotelListAPIUrl()).then(response => {
      if (!response.ok) {
        throw Error("Network request failed")
      }

      return response
    })
      .then(d => d.json())
      .then(d => {
        this.setState({hotelPrices: d})
      }, () => {
        this.setState({requestFailed: true})
      })
  }

  /**
   *
   * Render the component.
   *
   * 1) Check for the current state's requestFailed property. Show a different
   *    element
   *
   * @returns
   * @memberof HotelList
   */
  render() {
    if (this.state.requestFailed)
      return <p>Failed!</p>
    if (!this.state.hotelPrices)
      return <p>Loading Prices...</p>
    let hotelPrices = _.sortedUniqBy(this.state.hotelPrices.list.sort(this.compareHotelNames), 'name');
    return (
      <dl className="App-hotel-list">
        {hotelPrices.map(hotel => (
          <Fragment key={hotel.code}>
            <dt>
              <a href="">{hotel.name}</a>
            </dt>
            <dd>${hotel
                .price
                .toFixed(2)}</dd>
          </Fragment>
        ))}
      </dl>
    );
  }

  /**
   * Used to sort objects based on their "name" property
   *
   * TODO: Fix this overkill method (Possibly with a library lodash approach?)
   *
   * @param {*} a
   * @param {*} b
   * @returns
   * @memberof HotelList
   */
  compareHotelNames(a, b) {
    // Use toUpperCase() to ignore character casing
    const hotelA = a
      .name
      .toUpperCase();
    const hotelB = b
      .name
      .toUpperCase();

    let comparison = 0;
    if (hotelA > hotelB) {
      comparison = 1;
    } else if (hotelA < hotelB) {
      comparison = -1;
    }
    return comparison;
  }

}

export default HotelList;
